#!/bin/bash

# the local configuration file to be replaced / parsed
SABNZBD_CONFIG_LOCAL="/sabnzbd.ini"
NZBTOMEDIA_CONFIG_LOCAL="/nzbToMedia/autoProcessMedia.cfg"

# sabnzbd: check for configuration download
if [ -z "$SABNZBD_CONFIG_URL" ]; then 
  echo "sabnzbd: No config url set. Will use local configuration file"
else
  echo "sabnzbd: Config url is set. Download the configuration file"
  # now try to download the configuration file
  if [ -z "$SABNZBD_CONFIG_USERNAME" ] || [ -z "$SABNZBD_CONFIG_PASSWORD" ]; then
    # if no usename and password is specified
    curl "$SABNZBD_CONFIG_URL" -o "$SABNZBD_CONFIG_LOCAL"
    [ $? -ne 0 ] && exit 1
  else
    curl --user $SABNZBD_CONFIG_USERNAME:$SABNZBD_CONFIG_PASSWORD "$SABNZBD_CONFIG_URL" -o "$SABNZBD_CONFIG_LOCAL"
    [ $? -ne 0 ] && exit 1
  fi
fi

# first check if the download url is set. if so try to download the file via curl
if [ -z "$NZBTOMEDIA_CONFIG_URL" ]; then 
  echo "nzbtomedia: No config url set. Will use local configuration file"
else
  echo "nzbtomedia: Config url is set. Download the configuration file"
  # now try to download the configuration file
  if [ -z "$NZBTOMEDIA_CONFIG_USERNAME" ] || [ -z "$NZBTOMEDIA_CONFIG_PASSWORD" ]; then
    # if no usename and password is specified
    curl "$NZBTOMEDIA_CONFIG_URL" -o "$NZBTOMEDIA_CONFIG_LOCAL"
    [ $? -ne 0 ] && exit 1
  else
    curl --user $NZBTOMEDIA_CONFIG_USERNAME:$NZBTOMEDIA_CONFIG_PASSWORD "$NZBTOMEDIA_CONFIG_URL" -o "$NZBTOMEDIA_CONFIG_LOCAL"
    [ $? -ne 0 ] && exit 1
  fi
fi

# run sabnzbd
echo "Run sabnzbd"
/usr/bin/sabnzbdplus --server 0.0.0.0 --config-file $SABNZBD_CONFIG_LOCAL