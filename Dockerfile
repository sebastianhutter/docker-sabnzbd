FROM sebastianhutter/ffmpeg

# set workdir
WORKDIR /

# install sabnzbdplus
RUN echo "deb http://deb.debian.org/debian stable contrib non-free" > /etc/apt/sources.list.d/contrib.list \
  && apt-get update \
  && apt-get install -y sabnzbdplus git curl \
  && rm -rf /var/lib/apt/lists/*

# download nzbtomedia
RUN git clone https://github.com/clinton-hall/nzbToMedia.git

# set env vars for the configuration files
# lets create empty configuration files with the paths
ENV SABNZBD_CONFIG_LOCAL="/sabnzbd.ini"
ENV NZBTOMEDIA_CONFIG_LOCAL="/nzbToMedia/autoProcessMedia.cfg"
RUN touch ${SABNZBD_CONFIG_LOCAL} ${NZBTOMEDIA_CONFIG_LOCAL}

# add docker entrypoint
ADD build/docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

# run the docker entrypoint script
ENTRYPOINT ["/docker-entrypoint.sh"]
